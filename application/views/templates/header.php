<html>
	<head>
		<title><?=$logo;?></title>
		<link rel="stylesheet" type="text/css" href="<?= base_url('css/bootstrap.css'); ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('css/style.css'); ?>">
	</head>
	<body>
		<header class="page-header">
			<h1 class="title"><?=$logo?>
				<ul class="nav navbar-nav">
					<li><a href="/applications">applications</a></li>
					<li><a href="/about">about</a></li>
					<li><a href="/user_guide" target="_blank">help</a></li>
				</ul>
			</h1>
		</header>
		<section class="container">
			<h2 class="title"><?=$pagetitle;?></h2>