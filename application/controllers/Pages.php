<?php
	class Pages extends CI_Controller{

		public function view($page = 'home'){
			if( ! file_exists(APPPATH.'views/pages/'.$page.'.php')){
				show_404();
			}

			$data['logo'] = 'Apache Interface';
			$data['pagetitle'] = ucfirst($page);

			//dependencies
			$this->load->helper('url');
			$this->load->helper('directory');


			//view
			$this->load->view('templates/header', $data);
			$this->load->view('pages/'.$page, $data);

		}
	}
?>